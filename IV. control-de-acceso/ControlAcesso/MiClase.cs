﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtroNamespace
{
	class MiClase
	{
		// Puede ser visto por cualquier clase
		public void MetodoPublico()
		{
			Console.WriteLine("Llamando a metodo publico...");
		}

		// Puede ser visto desde esta clase o cualquier clase
		// que herede de MiClase
		protected void MetodoProtected()
		{
			Console.WriteLine("Llamando a metodo protected...");
		}

		//	Puede ser visto por cualquier clase dentro de un assembly
		/* assembly: Un programa en C# a la hora de distribuirse, se almacena en un ensamblado o assembly
		   este ensamblado es un empaquetado un conjunto de clases y namespaces que todos tienen algo en comun
		   y que se mandan para distribuir Ej: DDL pequeñas bibliotecas, pequeños conjuntos de codigo para alguna
			app en particular aqui van cargados los assembly que contienen su codigo */
		internal void MetodoInternal()
		{
			Console.WriteLine("Llamando a metodo internal...");
		}

		// Puede ser visto solo por esta clase
		private void MetodoPrivate()
		{
			Console.WriteLine("Llamando a metodo private...");
		}
	}
}
