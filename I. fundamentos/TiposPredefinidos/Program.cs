﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tipos_predefinidos
{
	class Program
	{
		static void Main(string[] args)
		{
			/*ENTEROS*/
			//long 64bits	
			long lo = 10500;
			//int 32bits
			int en = 10;
			//short 16bits
			short sh = 140;
			//byte 8 bits
			byte by = 10;

			Console.WriteLine("Long: {0}, Int: {1}, Short: {2}, Byte: {3}", lo, en, sh, by);
		
			/*FLOTANTES*/
			//System.Single
			float fl = 123.45f;
			//System.Double
			double dou = 3.123456789;
			//System.Decimal *Finanzas*
			decimal dec = 129.99m;

			Console.WriteLine("Float: {0}, Double {1}, Decimal: {2}", fl, dou, dec);

			/*OTROS*/
			//System.Char
			char ch = 'A';
			//System.Boolean
			bool boo = true;

			Console.WriteLine("Char: {0}, Bool: {1}", ch, boo);

		}
	}
}
