﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
	class Program
	{
		static void Main(string[] args)
		{
			Persona p = new Persona("Carlos", 38);
			p.Saludar();
			//p.Trabajar(); metodo de empleado, persona es la superclase pero no 
			//contiene el metodo trabajar en ella, sino que la tiene Empleado

			Empleado e = new Empleado("John", 29, "Ventas", 10000);
			e.Saludar();
			e.Trabajar();
		}
	}
}
