﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesAbstractas
{
	class Program
	{
		static void Main(string[] args)
		{
			EmpleadoHonorarios eh = new EmpleadoHonorarios("Simon", "Ventas", 12000);
			eh.Trabajar();
			Console.WriteLine("Salario por honorarios: {0}", eh.Salario + "$");

			EmpleadoNominas en = new EmpleadoNominas("Sandra", "RH", 12000);
			en.Trabajar();
			Console.WriteLine("Salario por nómina: {0}", en.Salario + "$");
		}
	}
}
