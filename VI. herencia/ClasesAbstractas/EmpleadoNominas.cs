﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Para que una clase que se hereda de una clase abstracta compile
* tiene que sobreescribirse todos los datos o debe declararse
* a si misma como abstracta
* Ej: abstract class EmpleadoNominas : Empleado { }
* Empleado es la clase abstracta y EmpleadoNomina es una
* clase que se define a si misma como abstracta*/
namespace ClasesAbstractas
{
	class EmpleadoNominas : Empleado
	{
		public decimal SueldoBase { get; set; }
		
		public override decimal Salario 
		{
			get
			{
				return SueldoBase;
			}
		}

		public EmpleadoNominas(string nombre, string puesto, decimal sueldoBase)
			: base(nombre, puesto)
		{
			SueldoBase = sueldoBase;
		}

		public override void Trabajar()
		{
			Console.WriteLine("Trabajando por nomina...");
		}
	}
}
