﻿using Herencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace RETO4
{
	class Estudiante : Persona
	{
		public double Promedio { get; set; }
		public string Grado { get; set; }

		public Estudiante(string nombre, int edad, double prom, string grado) 
			: base(nombre, edad)
		{
			Promedio = prom;
			Grado = grado;
		}

		public override void Saludar()
		{
			base.Saludar(); //Version persona
			Console.WriteLine("Mi nombre es {0} y mi promedio es {1:0.0}",
				Nombre, Promedio);
		}

		public void Estudiar()
		{
			Console.WriteLine("{0} está estudiando...",
				Nombre);
		}

		public void IrAClase()
		{
			Console.WriteLine("Mi clase es de {0}",
				Grado);
		}

		public override string ToString()
		{
			return string.Format("Nombre: {0}\nEdad: {1}\nPromedio: {2}\nGrado: {3}",
				Nombre, Edad, Promedio, Grado);
		}
	}
}
