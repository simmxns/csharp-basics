﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Ctrl + K & Ctrl + D autoidentacion
namespace AtajosTeclado
{
	class Program
	{
		static void Main(string[] args)
		{
			int miVariableSuperMil = 1000; //Ctrl + F ocurrencias
			Console.WriteLine(miVariableSuperMil);//Ctrl + D reemplazar ocurrencias
			miVariableSuperMil++;
			int a = miVariableSuperMil + 1;
			// ... mil operaciones
		}
		public static void MiMetodo()
		{
			//Console.WriteLine("123"); Ctrl + K & Ctrl + C poner comentarios
			//Console.WriteLine("123");     "    & Ctrl + U quitar comentarios
			//Console.WriteLine("123"); Ctrl + Ç alternar lo anterior
			//Console.WriteLine("123");
			//Console.WriteLine("123");
		}
		public static void Metodo2()
		{
			Console.WriteLine("Atajo super"); // cw doble tab autocompletado
			Console.WriteLine("es lo mejor");
			for (int i = 0; i < 10; i++) // for doble tab
			{
				Console.WriteLine(i);
			}
		}
		class MyClass //class doble tab
		{
			public int MiPropiedad { get; set; } //prop doble tab
		}
	}
}
