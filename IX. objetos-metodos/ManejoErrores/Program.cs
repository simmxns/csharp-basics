﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoErrores
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Console.Write("Ingresa el numerador: ");
				int numerador = int.Parse(Console.ReadLine());

				Console.Write("Ingresa el denominador: ");
				int denominador = int.Parse(Console.ReadLine());

				int resultado = numerador / denominador;
				Console.WriteLine("\nResultado: {0} / {1} = {2}", numerador, denominador, resultado);
			} 
			catch(FormatException formatexception)
			{
				Console.WriteLine("\n" + formatexception.Message);
				Console.WriteLine("Debes ingresar 2 enteros");
			}
			catch(DivideByZeroException de)
			{
				Console.WriteLine("\n" + de.Message);
				Console.WriteLine("Cero es un denominador invalido");
			}
			catch(Exception e)
			{
				Console.WriteLine("\n" + e.Message);
				Console.WriteLine("Ocurrio un error, vuelve a intentarlo...");
			}
		}
	}
}
