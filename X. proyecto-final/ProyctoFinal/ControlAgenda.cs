﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ProyctoFinal
{
	class ControlAgenda
	{
		private Agenda _agenda;
		
		public ControlAgenda(Agenda agenda)
		{
			_agenda = agenda;
		}

		#region Metodos
		public void VerContactos()
		{
			Limpiar();
			MenuMostrar();
			string opcion = Console.ReadLine();

			switch (opcion)
			{
				case "1":
					Console.WriteLine("Contactos en orden ASC");
					_agenda.MostrarOrdenados();
					break;
				case "2":
					Console.WriteLine("Contactos en orden DESC");
					_agenda.MostrarOrdenadosDesc();
					break;
				case "3":
					return;
				default:
					Console.WriteLine("Opción no válida");
					break;
			}
			PrecionaContinuar();
		}
		public void AgregarContacto()
		{
			Limpiar();
			Console.WriteLine("Nuevo Contacto");
			Contacto contacto = new Contacto();
			Console.Write("Nombre: ");
			contacto.Nombre = Console.ReadLine();
			Console.Write("Telefono: ");
			contacto.Telefono = Console.ReadLine();
			Console.Write("Correo: ");
			contacto.Correo = Console.ReadLine();

			_agenda.AgregarContacto(contacto);
			Console.WriteLine("Contacto agregado exitosamente");
			//presiona una tecla para continuar
			PrecionaContinuar();
		}
		public void BorrarUltimoContacto()
		{
			Limpiar();
			_agenda.BorrarUltimoContacto();
			Console.WriteLine("Contacto borrado exitosamente");
			PrecionaContinuar();
		}
		public void BuscarPorNombre()
		{
			Limpiar();
			Console.WriteLine("Buscar contacto");
			Console.Write("Nombre: ");
			string nombre = Console.ReadLine();
			//devuelve una instancia de Contacto
			Contacto contacto = _agenda.BuscarPorNombre(nombre);
			if(contacto != null)
			{
				Console.WriteLine("Datos: \n" + contacto);
			} else {
				Console.WriteLine("Contacto no encontrado");
			}
			PrecionaContinuar();
		}
		public static void AcercaDe()
		{
			Limpiar();
			Console.WriteLine("Simon Villafañe\n" +
				"+54 9 11 9999-9999\n" +
				"simon@dominio.com\n" +
				"ALL RIGHTS RESERVED");
			PrecionaContinuar();
		}
		public void MenuMostrar()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("1. Mostrar orden ascendente");
			sb.AppendLine("2. Mostrar descendente");
			sb.AppendLine("3. Volver al menu principal");
			sb.AppendLine("Seleccione una opcion");

			Console.Write(sb.ToString());
		}

		private static void PrecionaContinuar()
		{
			Console.WriteLine("Presiona cualquier tecla para continuar");
			Console.ReadKey();
		}
		private static void Limpiar()
		{
			Console.Clear();
		}
		#endregion
	}
}
