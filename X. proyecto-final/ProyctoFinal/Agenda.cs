﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ProyctoFinal
{
	class Agenda
	{
		private const int TAM = 10;
		private Contacto[] _contactos;
		private int _index;
		//prop para que otras clases conozcan cuantos contactos tienes almacenados
		public int NumContactos { get { return _index; } }

		public Agenda()
		{
			_index = 0;
			_contactos = new Contacto[TAM];
		}
		#region Metodos
		public void AgregarContacto(Contacto contacto)
		{
			if(_index < TAM)
			{
				_contactos[_index] = contacto;
				_index++;
			} else {
				Console.WriteLine("Agenda Llena");
			}
		}

		public void BorrarUltimoContacto()
		{
			if(_index > 0)
			{
				_contactos[_index] = null;
				_index--;
			} else {
				Console.WriteLine("La agenda ya está vacia");
			}
		}
		private bool NoHayContactos()
		{
			if(_index == 0)
			{
				Console.WriteLine("No Hay Contactos");
				return true;
			} return false;
		}
		public void MostrarOrdenados()
		{
			if (NoHayContactos()) { return; }
			//arreglo ordenado
			Contacto[] ordenados = new Contacto[_index];
			//copio el array contactos en ordenados dependiendo de la cantidad _index
			Array.Copy(_contactos, ordenados, _index);
			Array.Sort(ordenados);
			//mostrar el array
			Console.WriteLine(CadenaContactos(ordenados));
		}
		public void MostrarOrdenadosDesc()
		{
			if (NoHayContactos()) { return; }
			//arreglo ordenado
			Contacto[] ordenados = new Contacto[_index];
			//copio el array contactos en ordenados dependiendo de la cantidad _index
			Array.Copy(_contactos, ordenados, _index);
			Array.Sort(ordenados);
			//orden desc
			Array.Reverse(ordenados);
			//mostrar el array
			Console.WriteLine(CadenaContactos(ordenados));
		}
		public Contacto BuscarPorNombre(string nombre)
		{
			foreach(Contacto contacto in _contactos)
			{
				if(contacto != null && contacto.Nombre == nombre)
				{
					return contacto;
				}
			}
			return null;
		}
		public void MostrarContactos()
		{
			//tiene sobreescrito el metodo ToString
			Console.WriteLine(this);
		}
		public override string ToString()
		{
			return CadenaContactos(_contactos);
		}
		//sobreescritura de ToString() generacion de la cadena de contactos
		private string CadenaContactos(Contacto[] contactos)
		{
			//varios elementos que estan contenidos en un arreglo, 
			//no es buena practica usar la concatenacion de cadenas
			StringBuilder sb = new StringBuilder();
			for(int i=0; i<_index; i++)
			{
				//si el contacto es null me salto el paso
				if (contactos[i] == null) { continue; }
				string dato = string.Format("{0}. {1}", i + 1, contactos[i]);
				//añadiendo al StringBuilder todos los elementos del contacto
				sb.AppendLine(dato);
			}
			//regresamos el StringBuilder en forma de cadena
			return sb.ToString();
		}
		#endregion
	}
}
