﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyctoFinal
{
	class Contacto : IComparable<Contacto>
	{
		public string Nombre { get; set; }
		public string Telefono { get; set; }
		public string Correo { get; set; }

		public Contacto() { }

		public Contacto(string nombre, string telefono, string correo)
		{
			Nombre = nombre;
			Telefono = telefono;
			Correo = correo;
		}

		public override bool Equals(object obj)
		{
			//si el objeto es nulo devuelve falso
			if (obj == null){ return false; }
			//si no es nulo por consiguiente crea el objeto de tipo contacto
			Contacto c = obj as Contacto;
			//reviso si el objeto 'c' no es nulo
			if(c == null) { return false; }
			//determino si un objeto de nombre y telefono son de mismo valor
			return string.Equals(Nombre, c.Nombre) && string.Equals(Telefono, c.Telefono);
		}
		public override int GetHashCode()
		{
			//asegura que nuestro calculo no se desborde
			unchecked
			{
				//si nombre no es nulo obtengo su codigo de hash sino devuelvo 0
				int hashNombre = (Nombre != null ? Nombre.GetHashCode() : 0);
				int hashTelefono = (Telefono != null ? Telefono.GetHashCode() : 0);
				//operacion a nivel de bits
				return (hashNombre * 397) ^ (hashTelefono);
			}
		}
		//formato para string
		public override string ToString()
		{
			return string.Format("Nombre: {0}\nTelefono: {1}\nCorreo: {2}\n", Nombre, Telefono, Correo);
		}
		//metodo para poder ordenar elementos de tipo contacto en un arreglo
		public int CompareTo(Contacto otro)
		{
			return Nombre.CompareTo(otro.Nombre);
		}
	}
}
