﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Crear un programa que contenga las siguentes opciones:
 - Ver contactos
 - Agregar contactos
 - Borrar ultimo contacto
 - Buscar contacto por nombre
 - Acerca de
 - Salir
 Cada una con su respectiva funcionalidad
 A tener en cuenta: 
 # Al ver los contactos se tiene que poder ver en orden ascendente o descente
 # Clase contacto: Nombre, Telefono, Email
 # Agenda como estructura de almacenamiento puede usar un maximo de 20 elementos en el arreglo
 # Comparar instancias de la Clase contacto
 # El programa debe cumplir con un diseño orientado a objetos
*/
namespace ProyctoFinal
{
	class Program
	{
		//variable de tipo ControlAgenda
		static ControlAgenda control = new ControlAgenda(new Agenda());

		static void Main(string[] args)
		{
			string opcion = "";

			do
			{
				Console.Clear();
				Console.WriteLine("Agenda de Contactos");
				ImprimeMenu();
				opcion = Console.ReadLine();
				switch (opcion)
				{
					case "1":
						control.VerContactos();
						break;
					case "2":
						control.AgregarContacto();
						break;
					case "3":
						control.BorrarUltimoContacto();
						break;
					case "4":
						control.BuscarPorNombre();
						break;
					case "5":
						ControlAgenda.AcercaDe();
						break;
					case "6":
						break;
					default:
						Console.WriteLine("Opcion no valida");
						break;
				} 
			} while (opcion != "6");
		}

		static void ImprimeMenu()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("1. Ver Contactos");
			sb.AppendLine("2. Agregar Contacto");
			sb.AppendLine("3. Borrar Ultimo Contacto");
			sb.AppendLine("4. Buscar Contacto por Nombre");
			sb.AppendLine("5. Acerca De");
			sb.AppendLine("6. Salir");
			sb.Append("Seleccione una opcion: ");

			Console.Write(sb.ToString());
		}
	}
}
