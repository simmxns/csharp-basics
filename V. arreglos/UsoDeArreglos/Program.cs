﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsoDeArreglos
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Tamaño del Array: ");
			int tam = Convert.ToInt32(Console.ReadLine());

			int[] arreglo = new int[tam];
			//Pedir datos del arreglo
			for (int i = 0; i < arreglo.Length; i++)
			{
				Console.Write("Escribe numero en indice {0}: ", i);
				arreglo[i] = Convert.ToInt32(Console.ReadLine());
			}
			//Muestro los datos mediante el metodo
			MostrarDatos(arreglo);
		}
		//Metodo MostrarDatos
		static void MostrarDatos(int[] arreglo)
		{
			foreach(int num in arreglo)
			{
				Console.Write("{0} ", num);
			}
			Console.Write("\n");
		}
	}
}
