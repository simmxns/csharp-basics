﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseArray
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] list = { 34, 72, 13, 44, 25, 30, 10 };
			int[] temp = new int[list.Length];

			//Generar una copia del arreglo list
			Array.Copy(list, temp, list.Length);

			Console.Write("Arreglo Original: ");
			MostrarDatos(list);
			Console.Write("Arreglo Copiado: ");
			MostrarDatos(temp);

			//Invierte el arreglo
			Array.Reverse(temp);
			Console.Write("Arreglo invertido: ");
			MostrarDatos(temp);

			//Ordenar el arreglo
			Array.Sort(list);
			Console.Write("Arreglo ordenado: ");
			MostrarDatos(temp);
		}
		//Metodo MostrarDatos
		static void MostrarDatos(int[] arreglo)
		{
			foreach (int num in arreglo)
			{
				Console.Write("{0} ", num);
			}
			Console.Write("\n");
		}
	}
}
