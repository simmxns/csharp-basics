﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RETO3
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Cantidad de calificaciones: ");
			int cant = Convert.ToInt32(Console.ReadLine());

			int[] cal = new int[cant];
			double suma = 0;
			for(int i=0; i < cal.Length; i++)
			{
				Console.Write("Calificacion {0}: ", i+1);
				cal[i] = int.Parse(Console.ReadLine());
				//SumaFor
				suma += cal[i]; 
			}
			Console.WriteLine("SumaFor");
			double promedio = suma / cal.Length;
			Console.WriteLine("Promedio: {0}", promedio);
			//SumaFuncion
			Console.WriteLine("SumaFuncion");
			double suma2 = cal.Sum();
			//Promedio
			double promedio2 = suma2 / cal.Length;
			Console.WriteLine("Promedio: {0}", promedio2);
		}
	}
}