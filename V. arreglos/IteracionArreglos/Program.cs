﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteracionArreglos
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] arreglo = { 10, 9, 30, -4, 0, 2, 11 };
	
			Console.WriteLine("FOR");
			for(int i=0; i < arreglo.Length; i++)
			{
				Console.Write("{0} ", arreglo[i]);
			}
			Console.Write("\n");

			Console.WriteLine("FOREACH");
			foreach(int num in arreglo)
			{
				Console.Write("{0} ", num);
			}
			Console.Write("\n");

			Console.WriteLine("FOR INVERSO");
			string[] cadenas = { "hola", "mundo", "ciclos", "arreglos", "cadenas" };
			for(int i=cadenas.Length - 1; i >= 0; i--)
			{
				Console.Write("{0} ", cadenas[i]);
			}
			Console.Write("\n");

			//Fuera del curso
			Console.WriteLine("WHILE");
			int j = 0;
			while(j < arreglo.Length)
			{
				Console.Write("{0} ", arreglo[j]);
				j++;
			}
			Console.Write("\n");
		}
	}
}
