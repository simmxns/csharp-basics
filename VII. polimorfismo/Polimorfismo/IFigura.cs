﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
	/* Todos los miembros de una figura
	 * son abstractos
	 * Todos los miembros de una interface
	 * son publicos */
	interface IFigura
	{
		//propiedad
		double CalcularArea();
	}
}
