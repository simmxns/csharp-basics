﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
	class Circulo : IFigura
	{
		public double Radio { get; set; }

		public double CalcularArea()
		{
			return Math.PI * Radio * Radio;
		}
		/* Para las interfaces no hacen falta escribir 
		 * override a la hora de implementar o sobreescribir 
		 * el metodo*/
		
		public void DatosCirculos()
		{
			Console.WriteLine("Radio: {0}", Radio);
		} 
	}
}
