﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompararObjetos
{
	class Producto : IComparable<Producto>
	{
		public string Codigo { get; set; }
		public decimal Precio { get; set; }

		public int CompareTo(Producto otro)
		{
			//> 0 mayor
			//0 == igual
			//< 0 menor
			if (this.Precio > otro.Precio) return 1;
			if (this.Precio == otro.Precio) return 0;
			return -1;
		}
	}
}
