﻿using constructores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosDeInstancia
{
	class Program
	{
		static void Main(string[] args)
		{
			CuentaBancaria cuenta1 = new CuentaBancaria("001", "Simon", 100);
			CuentaBancaria cuenta2 = new CuentaBancaria("002", "Sandra", 250);

			//Llamando a los metodos depositar y retirar
			Console.WriteLine("Depositar $70 en cuenta1");
			cuenta1.Depositar(70);

			Console.WriteLine("Retirar $20 en cuenta2");
			cuenta2.Retirar(20);

			Console.WriteLine(cuenta1.ToString());
			Console.WriteLine(cuenta2);
		}
	}
}
