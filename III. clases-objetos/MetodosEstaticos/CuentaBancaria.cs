﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace constructores
{
	class CuentaBancaria
	{
		//Auto properties (propiedades automaticas)
		public string NoCuenta { get; set; }
		public string Usuario { get; set; }

		// Backing field
		private decimal _saldo;

		//Full property (propiedad completa)
		public decimal Saldo
		{
			get { return _saldo; }
			set
			{
				//Evitar que el saldo sea negativo
				/*if (value < 0)
				{
					_saldo = 0;
				}
				else
				{
					_saldo = value;
				}*/

				_saldo = value < 0 ? 0 : value;
			}
		}

		public static string Banco { get; set; }

		#region Constructores
		public CuentaBancaria(string noCuenta)
		{
			NoCuenta = noCuenta;
		}
		public CuentaBancaria(string noCuenta, string usuario)
			: this(noCuenta)
		{
			Usuario = usuario;
		}
		public CuentaBancaria(string noCuenta, string usuario, decimal saldo)
			: this(noCuenta, usuario)
		{
			Saldo = saldo;
		}

		public CuentaBancaria(){ }
		#endregion

		#region Metodos de Instancia
		public void Retirar(decimal cantidad)
		{
			if(cantidad > Saldo)
			{
				Console.WriteLine("Saldo insuficiente");
				return;
			}
			Saldo -= cantidad;
		}

		public void Depositar(decimal cantidad)
		{
			Saldo += cantidad;
		}

		public override string ToString()
		{
			return string.Format("NºCuenta: {0}, Usuario: {1}, Saldo: ${2}", NoCuenta, Usuario, Saldo);
		}
		#endregion

		public static void AsignarBanco(string banco)
		{
			Console.WriteLine("Asignando nuevo banco...");
			Banco = banco;
		}
	}
}