﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto2
{
	class Persona
	{
		//Auto properties
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		//Backing field
		private int _edad;

		//Validar edad
		public int Edad
		{
			get { return _edad; }
			set
			{
				_edad = value < 0 ? 0 : value;
			}
		}

		#region Constructores
		public Persona(string nombre, string apellido, int edad)
		{
			Nombre = nombre;
			Apellido = apellido;
			Edad = edad;
		}
		#endregion

		#region Metodos
		public void Saludar()
		{
			Console.WriteLine("Hola! {0} {1}", Nombre, Apellido);
		}

		public override string ToString()
		{
			return string.Format("Nombre: {0}\nApellido: {1}\nEdad: {2}", Nombre, Apellido, Edad);
		}
		#endregion

	}
}
