﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto2
{
	class Program
	{
		static void Main(string[] args)
		{
			Persona persona1 = new Persona("Simon", "Villafane", 17);
			Persona persona2 = new Persona("Sandra", "De Villafane", 17);
			
			persona1.Saludar();
			Console.WriteLine(persona1);
			Console.Write("\n");
			persona2.Saludar();
			Console.WriteLine(persona2);
		}
	}
}
