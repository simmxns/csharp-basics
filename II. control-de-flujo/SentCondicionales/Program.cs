﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sent_condicionales
{
	class Program
	{
		static void Main(string[] args)
		{
			/* IF */
			int num = 4;
			if (num < 5) {
				Console.WriteLine("Es menor");
			}

			/* Condicional Compuesta */
			string user = "user";
			string pass = "12345";

			if (user == "user" && pass == "12345") {
				Console.WriteLine("Iniciaste sesion");
			}

			/* IF ELSE */
			int edad = 23;
			if (edad >= 18) {
				Console.WriteLine("Mayor de edad");
			} else {
				Console.WriteLine("Menor de edad");
			}

			/* IF ELSE IF */
			int a = 1;
			int b = 2;
			if(a > b) {
				Console.WriteLine("{0} > {1}", a, b);
			} else if (a < b) {
				Console.WriteLine("{0} < {1}", a, b);
			} else {
				Console.WriteLine("{0} = {1}", a, b);
			}

			/* SWITCH */
			string cod = "A2";
			switch (cod){
				case "A1":
					Console.WriteLine("Prod 1");
					break;
				case "A2":
					Console.WriteLine("Prod 2");
					break;
				case "A3":
					Console.WriteLine("Prod 3");
					break;
			}
		}
	}
}
