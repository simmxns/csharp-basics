﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bucles
{
	class Program
	{
		static void Main(string[] args)
		{
			/* FOR */
			for (int i=0; i<=4; i++){
				Console.WriteLine("FOR: {0}", i);
				}

			Console.Write("\n");

			/* WHILE */
			int n = 0;
			while (n<=4){
				n++;
				Console.WriteLine("WHILE: {0}", n);
			}

			Console.Write("\n");

			/* DO WHILE */
			int c = 0;
			do{
				c++;
				Console.WriteLine("DO WHILE: {0}", c);
			} while (c < 5);
		}
	}
}
