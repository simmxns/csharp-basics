﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bucles2
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Break");
			for(int i=0; i<10; i++){
				Console.WriteLine("i: {0}", i);
				if(i == 5){
					break; //Termina el ciclo
				}
			}

			Console.WriteLine("\nContinue");
			int n = 0;
			while(n < 10){
				if(n % 2 == 0){
					n++;
					continue; //Salta las lineas que estén dentro del ciclo
				}
				Console.WriteLine("n: {0}", n);
				n++;
			}


		}
	}
}
