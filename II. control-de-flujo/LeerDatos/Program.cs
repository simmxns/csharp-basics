﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace leerDatos
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Suma de dos numeros");
			Console.Write("Ingresa un numero 1: ");
			//Los datos se leen como cadena
			string dato = Console.ReadLine();
			//Convertir a numero int.Parse
			int n1 = int.Parse(dato);
	
			Console.Write("Ingresa numero 2: ");
			dato = Console.ReadLine();
			int n2 = int.Parse(dato);

			int suma = n1 + n2;
			Console.WriteLine("{0} + {1} = {2}", n1, n2, suma);
		}
	}
}
